import React from 'react';
import Footer from '../../organisms/footer/Footer';

require('./_biographie.css');

export default function Biographie() {
    return (
        <>
            <div className='biographie__container'>
                <div className='biographie'>
                    <video className="videososvet" src='/videos/video-1.mp4' autoPlay loop muted />
                    <div className='photo'>
                        <p className="bio-2">Je m'appelle Miguel Garcia Mata, je suis Vénézuélien et je vis en France depuis 2 ans. J’ai quitté mon pays à la fin de l'année 2017.<br></br>
                        Au Venezuela j'ai étudié l’informatique (équivalent au master II en France). J’ai choisi ce métier par passion.
                        Ensuite, j’ai travaillé chez INVERMATA pendant 5 ans. INVERMATA est un Fournisseur Agro-alimentaire (achat de nourriture et de médicaments en gros puis revente aux commerçants locaux de la région de Margarita).
                        </p>
                    </div>

                    <h1 className="texte-biographie" >Biographie</h1>
                    <div className="bio">
                        <p>
                            Au sein de cette entreprise, j'ai été au départ assistant informatique en charge de la maintenance du réseau et de l’ensemble des logiciels.
                        J'ai ensuite évolué vers un poste de responsable commercial. Par la suite j'ai été promus Directeur Général en charge de l’entreprise, du management  d’une equipe du cinquantaine collaborateurs et du dévéloppement du business.<br></br>
                        Quand je suis arrivé en France, j'ai appris le français puis j’ai rapidement trouvé un poste chez Intermarché à Paris comme chargé de rayon
                        alimentaire : gestion des commandes, suivi des stocks, merchandising, promotions, etc. J’ai ensuite décidé de reprendre des études en informatique
                        chez SIMPLON où j'ai finalisé ma formation intensive de développeur fullstack Javascript avec l'obtention du diplôme de niveau 3.<br></br>
                        Je suis actuellement à la recherche d’un emploi comme développeur web junior.<br></br>
                        Mes différentes formations techniques au Vénézuela puis chez SIMPLON m’ont permis d’acquérir des connaissances techniques solides : JAVASCRIPT, HTML, CSS, JAVA, PHP, MySQL, C++, C, ReactJS, PostgreSQL, Angular... <br></br>
                        Par ailleurs, je suis d’un naturel ouvert et curieux, très sociable et toujours enthousiaste dans mon travail.
                        J’ai un grand intérêt à travailler dans une entreprise, ce qui me permettra de surmonter des défis personnels et professionnels de manière constante.
                        </p>
                    </div>
                </div>
            </div>

            <Footer />
        </>
    )
}